
public class Program {
	
	public static void main(String[] args){
		//television();
		//pairOfDice();
		//heartRate();
		//billing();
		//savingBalance();
		myDate();
		
	}
	
	//Questiion 1; 
	
	static void television(){
		Television tv = new Television("Sony", 25);
		tv.powerOn();
		tv.setChannel(50);
		System.out.println("Manufacturer = "+tv.getManufacturer());
		System.out.println("Channel = "+tv.getChannel());
		System.out.println("Screen Size = "+tv.getScreenSize());
		tv.increaseVolume();
		System.out.println("volumn = "+tv.getVolumn());
	}
	
	// Question 2;
	static void pairOfDice(){
		int count = 1;
		while(true){
			PairOfDice dice1 = new PairOfDice();
			PairOfDice dice2 = new PairOfDice();
			int firstPair = dice1.die1+dice1.die2;
			int secondPair = dice2.die1+ dice2.die2;
			System.out.println("First pair: "+firstPair);
			System.out.println("Second pair: "+secondPair);
			if(firstPair == secondPair){
				System.out.println("It took "+count+" times to reach the same value");
				break;
			}
			count++;	
		}
	}
	/*
	 * output
	 * First pair: 1
		Second pair: 3
		First pair: 5
		Second pair: 7
		First pair: 5
		Second pair: 6
		First pair: 3
		Second pair: 3
		It took 4 times to reach the same value
	 * */
	
	//Question 3
	static void heartRate(){
//		int beatsPerMinute = 220;
//		int age  = 25;
//		int mhr = beatsPerMinute - age;
//		int rhr = 70;
//		int ahr = mhr - rhr;
//		double lb = 0.5;
//		double ub = 0.85;
//		double lbthr = (ahr*lb) + rhr;
//		double ubthr = (ahr * ub) + rhr;
//		
//		System.out.println(lbthr);
//		System.out.println(ubthr);
		
		HeartRates hr = new HeartRates("Bibash", "Kafle", "02-26-1991");
		System.out.println("Name = "+hr.getFirstName()+" "+hr.getLastName());
		System.out.println("Age = "+hr.getAge());
		System.out.println("UBTHR = "+hr.getUpperBoundaryTargetHeartRate());
		System.out.println("LBTHR = "+hr.getLowerBoundaryTargetHeartRate());

	}
	
	/* Output
	 *  Name = Bibash Kafle
		Age = 25
		UBTHR = 176.25
		LBTHR = 132.5
	 */
	
	//Question 4
	static void billing(){
		Billing bill = new Billing();
		System.out.println("input = {price:200}");
		System.out.println("Result = "+ bill.computeBill(200));
		System.out.println("input = {price:220, qty:10}");
		System.out.println("Result = "+ bill.computeBill(220, 10));
		System.out.println("input = {price:200, qty:10, coupon:2}");
		System.out.println("Result = "+ bill.computeBill(220, 10,2));
	}
	/* input / output
	 * 
	 * input = {price:200}
		Result = 216.0
		
		input = {price:220, qty:10}
		Result = 2376.0
		
		input = {price:200, qty:10, coupon:2}
		Result = 2328.48
	 */
	
	//Question 5
	static void savingBalance(){
		
		SavingsAccount saver1 = new SavingsAccount(2000);
		SavingsAccount saver2 = new SavingsAccount(3000);
		
		System.out.println("Saver1 opening balance = "+saver1.getSavingsBalance());
		System.out.println("Saver2 opening balance = "+saver2.getSavingsBalance());
		
		SavingsAccount.setAnnualInterestRate(4);
		for (int i = 0; i < 12; i++) {
			saver1.calculateMonthlyInterest();
			saver2.calculateMonthlyInterest();
		}
		
		System.out.println("Interest Rate = "+SavingsAccount.getAnnualInterestRate());
		System.out.println("Saver1 total balance = "+saver1.getSavingsBalance());
		System.out.println("Saver2 total balance = "+saver2.getSavingsBalance());
		
		
		SavingsAccount.setAnnualInterestRate(5);
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println("Interest Rate = "+SavingsAccount.getAnnualInterestRate());
		System.out.println("Saver1 total balance = "+saver1.getSavingsBalance());
		System.out.println("Saver2 total balance = "+saver2.getSavingsBalance());
		
	}
	/*
	 * input / output
	 * 
	 *  Saver1 opening balance = 2000.0
		Saver2 opening balance = 3000.0
		
		Interest Rate = 4.0
		Saver1 total balance = 2081.4830858395785
		Saver2 total balance = 3122.2246287593684
		
		Interest Rate = 5.0
		Saver1 total balance = 2090.155932030577
		Saver2 total balance = 3135.2338980458658
	 */
	
	static void myDate(){
		MyDate date = new MyDate(1, 11, 1991);
		MyDate date1 = new MyDate("Feb",12, 1991);
		MyDate date2 = new MyDate(110,2015);
		date.getDate();
		/*System.out.println(date.getDate());
		System.out.println(date1.getDate());*/
	}
}
