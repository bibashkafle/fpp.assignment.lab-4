
public class SavingsAccount {
	private static double annualInterestRate;
	private double savingsBalance;
	
	public SavingsAccount(double savingBalance){
		this.savingsBalance = savingBalance;
	}
	
	public void calculateMonthlyInterest(){
		double interest = (this.savingsBalance*SavingsAccount.getAnnualInterestRate())/100;
		interest = interest/12;
		this.savingsBalance += interest;
	}
	
	public double getSavingsBalance() {
		return this.savingsBalance;
	}

	public static double getAnnualInterestRate() {
		return annualInterestRate;
	}

	public static void setAnnualInterestRate(double annualInterestRate) {
		SavingsAccount.annualInterestRate = annualInterestRate;
	}
}
