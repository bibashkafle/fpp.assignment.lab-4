import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class HeartRates {
	
	 private int beatsPerMinute = 220;
	 private int restingHeartRate = 70;
	 private double lb = 0.5;
	 private double ub = 0.85;
	 
	 private String firstName;
	 private String lastName;
	 private LocalDate dateOfBirth;
	 
	 /***
	  * 
	  * @param firstName 
	  * @param lastName
	  * @param dateOfBirth [Format MM-dd-yyyy]
	  */
	 public HeartRates(String firstName, String lastName, String dateOfBirth){
		 this.setFirstName(firstName);
		 this.setLastName(lastName);
		 this.dateOfBirth =  LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("MM-dd-yyyy"));
	 }
	 
	 public int getMaxHeartRate(){
		 return this.beatsPerMinute - this.getAge();
	 }
	 
	 public int getAverageHeartRate(){
		 return this.getMaxHeartRate()-restingHeartRate;
	 }
	 
	 public double getLowerBoundaryTargetHeartRate()
	 {
		 return (this.getAverageHeartRate()*this.lb) + this.restingHeartRate;
	 }
	 
	 public double getUpperBoundaryTargetHeartRate()
	 {
		 return (this.getAverageHeartRate()*this.ub) + this.restingHeartRate;
	 }
	 
	 public int getAge(){
		 LocalDate today = LocalDate.now();
		 Period p = Period.between(this.dateOfBirth, today);
		 return p.getYears();
	 }
	 
	 public double getUb(){
		 return this.ub;
	 }
	 
	 public double getlb(){
		 return this.lb;
	 }

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}
