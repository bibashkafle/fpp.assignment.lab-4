
public class Billing {

	public double computeBill(double price){
		double tax = (price*8)/100;
		return price+tax;
	}
	
	public double computeBill(double price, int quantity){
		double totalPrice = price*quantity;
		double tax = (totalPrice*8)/100;
		return totalPrice+tax;
	}
		
	public double computeBill(double price, int quantity,int coupon){
		double totalPrice = price*quantity;
		double deduction =  (totalPrice*coupon)/100.0;
		totalPrice = totalPrice - deduction;
		double tax = (totalPrice*8)/100;
		return totalPrice+tax;
	}
	
}
